function onLocationSuccess(location) {
    // location recording time
    var locationTime = Date(location.timestamp);
    // device's latitude
    var myLatitude = location.coords.latitude;
    // device's longitude
    var myLongitude = location.coords.longitude;
    // device's altitude - depends on device specs and support
    var myAltitude = location.coords.altitude;
    // device's heading - depends on device specs and support
    var myHeading = location.coords.heading;
    // output test to console...
    console.log("found location successfully!")
    // output result to #location div...

    $("#latitudeValue").val(myLatitude);
    $("#longitudeValue").val(myLongitude);
    $("#useCurrentLocation").val('true');
    //setting current location to text
    $("#locationName").val('Current Location('+myLatitude+','+myLongitude+')');
    
}

function onLocationFail(error) {
    $("#locationError").popup("open");
    $("#locationErrorText").val('Failed to get location. Please enter manually');
}

function getLocation() {
    console.log('getLocation - js call');
    // resetting values
    $("#useCurrentLocation").val('false');
    $("#locationName").val('');
    navigator.geolocation.getCurrentPosition(onLocationSuccess, onLocationFail,
            {
                timeout : 5000,
                enableHighAccuracy : true
            });
}

function onPictureSuccess(imageData) {
    console.log('success - js call');
    // JS selector call is slightly faster...
    var image = document.getElementById('imageView');
    image.src = imageData;
    console.log(imageData);
}

function onPictureFail(message) {
    alert('Failed because: ' + message);
}

function getPhoto(camera) {
    if (camera === true) {
        console.log('entering camera true');
        // Use from Camera
        navigator.camera.getPicture(onPictureSuccess, onPictureFail, {
            quality : 50,
            correctOrientation : true,
            sourceType : Camera.PictureSourceType.CAMERA,
            destinationType : Camera.DestinationType.FILE_URI
        });
    } else {
        console.log('entering camera false');
        navigator.camera.getPicture(onPictureSuccess, onPictureFail, {
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            destinationType : Camera.DestinationType.FILE_URI
        });
    }
}

function searchFoodSuccess(res) {
            console.log("starting daily note callback");
            var noteOutput = "Today's fun asynchronous note: "+ res;
            console.log(noteOutput);
}
        
function searchFoodFailure(results){

    console.log("Failed food search"+ results);

}
function getFoodNearby(){
    
    console.log('inside getFoodNearby:'+ $("#latitudeValue").val());
    var latitudeValue = $("#latitudeValue").val();
    var longitudeValue = $("#longitudeValue").val();
    var locationNameValue = $("#locationName").val();
    console.log('inside getFoodNearby'+ $("#useCurrentLocation").val())
    if($("#useCurrentLocation").val() === 'true'){
        console.log('using current location');
    }else{
        console.log('Not using current location'+ locationNameValue);
        var mySplitResult = locationNameValue.split(",");
        if(mySplitResult.length>0){
            latitudeValue = mySplitResult[0];
            longitudeValue = mySplitResult[1];    
        }
        

    }
    console.log(latitudeValue);
    console.log(longitudeValue);
    var noteSync = foodies.searchFoodNearCoords(Number(latitudeValue), Number(longitudeValue), searchFoodSuccess,searchFoodFailure);
}


/*var $jsonOnlineRequest; 

function searchFoodData(){

    $jsonOnlineRequest = $.getJSON("https://bitbucket.org/rayyanjaweed/jsonrepo/raw/cb298db036963300b04118d0b64f171f99c12168/searchResultJSON.json", function(data){

          console.log("Online API called");
          $('#searchList').append('<li data-role="list-divider" role="heading">Burgers around Chicago</li>');
          $.each(data, function (index, value) {
              $('#searchList').append('<li> <a href="#reviewPopup'+value.foodid+'" data-rel="popup" class="ui-btn ui-btn-d ui-icon-star ui-btn-icon-right" id="button'+value.foodid+'" data-transition="slide"><h3>' + value.name + '</h3><div data-role="controlgroup" data-type="horizontal">  <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> </div></a></li>');
              $('#searchList').append('<li><a href="'+ value.href_navigation +'" class="ui-btn ui-icon-navigation ui-btn-icon-right" ><p>'+value.price+'</p><p><strong>'+value.restaurantName + '</strong></p><p>'+ value.description+'</p></a></li>');
              $('#searchList').append('<div data-role="popup" id="reviewPopup'+value.foodid+'" class="ui-content" data-theme="a" ><a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" data-rel="back">Delete Icon</a><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><a href="#reviewPage" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-notext" data-transition="slide">Comment Icon</a></div>');
              $("#reviewPopup"+value.foodid).css({"width": "200"});
              $("#button"+value.foodid).click(function () {
                  $("#reviewPopup"+value.foodid).popup("open");
              });
          });

    });
}


    //Update indexed db after fetching data from online API service
    $jsonOnlineRequest.then(function(){
          var db;
          //Opening database in indexedDB
          var openDB = indexedDB.open("FoodiesDB",2);
          console.log("indexedDB.open is called ");
          openDB.onsuccess = function(e){
              db = e.target.result;
              openTransaction();
          }
          openDB.onerror = function(){
              console.log("openDB.onerror is called");
          }
          //Creating dataStore in indexedDB and upgrade of indexedDB
          openDB.onupgradeneeded = function(e){
              console.log("openDB.onupgradeneeded is called");
              var upgradeDB = e.target.result;
              if (!upgradeDB.objectStoreNames.contains("Food")) {
                  var dataStore = upgradeDB.createObjectStore("Food", { keyPath: "foodid" });
                  dataStore.createIndex("foodid","foodid", {unique:true});
                  console.log("Created objectStore and index");
              }
          }

          function openTransaction(){
              $.getJSON("https://bitbucket.org/rayyanjaweed/jsonrepo/raw/cb298db036963300b04118d0b64f171f99c12168/searchResultJSON.json", function(data){
                    var dataStore = db.transaction(["Food"],"readwrite").objectStore("Food");
                    for(i=0; i < data.length; i++){
                        dataStore.add(data[i]);
                    }
              });
          }
    });

    //Upon failing of online API service, fetch data from local indexed db
    $jsonOnlineRequest.fail(function(){
          console.log("Online JSON request failed");
          //$('#searchResultsPage').append('<script type="text/javascript" src="assets/scripts/jquery-2.1.4.min.js"></script> <script type="text/javascript" src="assets/scripts/jquery.mobile-1.4.5.min.js"></script> <script type="text/javascript" src="assets/scripts/index.js"></script> ');
          $('#searchList').append('<li data-role="list-divider" role="heading">Burgers around Chicago</li>');
          var db;
          //Opening database in indexedDB
          var openDB = indexedDB.open("FoodiesDB",2);
          // create transaction
          openDB.onsuccess = function(e){
              db = e.target.result;
              var dataStore2 = db.transaction(["Food"],"readonly").objectStore("Food");
                    // define data object store
                    var cursor = dataStore2.openCursor();
                    // do something with return...
                    cursor.onsuccess = function(e) {
                        var result = e.target.result;
                        if (result) {
                  $('#searchList').append('<li> <a href="#reviewPopup'+result.value.foodid+'" data-rel="popup" class="ui-btn ui-btn-d ui-icon-star ui-btn-icon-right" id="button'+result.value.foodid+'" data-transition="slide"><h3>' + result.value.name + '</h3><div data-role="controlgroup" data-type="horizontal">  <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> </div></a></li>');
                  $('#searchList').append('<li><a href="'+ result.value.href_navigation +'" class="ui-btn ui-icon-navigation ui-btn-icon-right" ><p>'+result.value.price+'</p><p><strong>'+result.value.restaurantName + '</strong></p><p>'+ result.value.description+'</p></a></li>');
                  $('#searchList').append('<div data-role="popup" id="reviewPopup'+result.value.foodid+'" class="ui-content" data-theme="a" ><a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" data-rel="back">Delete Icon</a><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><a href="#reviewPage" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-notext" data-transition="slide">Comment Icon</a></div>');
                  $("#reviewPopup"+result.value.foodid).css({"width": "200"});
                  $("#button"+result.value.foodid).click(function () {
                      $("#reviewPopup"+result.value.foodid).popup("open");
                  });
                  //$('#searchList').append('<li><a href="#reviewPopup'+result.value.foodid+'"  data-rel="popup" class="ui-btn ui-btn-d ui-icon-star ui-btn-icon-right" data-transition="slide"> <h3>' + result.value.name + '</h3> <div data-role="controlgroup" data-type="horizontal">  <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> </div> </a></li>');
                  //$('#searchList').append('<li><a href="'+ result.value.href_navigation +'" class="ui-btn ui-icon-navigation ui-btn-icon-right" ><p>'+result.value.price+'</p><p><strong>'+result.value.restaurantName + '</strong></p><p>'+ result.value.description+'</p></a></li>');
                  //$('#searchList').append('<div data-role="popup" id="reviewPopup'+result.value.foodid+'" class="ui-content" > <div data-role="controlgroup" data-type="horizontal"> <div>    <a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" data-rel="back">Delete Icon</a> <input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /> <input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /> <input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /> <input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><a href="#reviewPage" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-notext" data-transition="slide">Comment Icon</a></div></div></div>');
                //  $("#button"+result.value.foodid).click(function () {
                //      $("#reviewPopup1").popup("open");
                //  });
                        result.continue();
                        }
                    }
          }

    });*/