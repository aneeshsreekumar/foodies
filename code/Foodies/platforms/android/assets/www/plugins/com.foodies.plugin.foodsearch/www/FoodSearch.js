cordova.define("com.foodies.plugin.foodsearch.FoodSearch", function(require, exports, module) { var map;
var infowindow;
var successCallBack;
var failureCallBack;

function initMap(latitude,longitude,successMethod,failureMethod) {
  //{lat: 42.153085, lng: -88.007031};
  //29.829906, -97.982977
  successCallBack = successMethod;
  failureCallBack = failureMethod;
  console.log('came inside initMap');
  console.log(':'+latitude+':');
  console.log(':'+longitude+':');
  var ourlocation = {lat: latitude, lng: longitude};

  map = new google.maps.Map(document.getElementById('map'), {
    center: ourlocation,
    zoom: 15
  });
  console.log('came after getElementById initMap');
  infowindow = new google.maps.InfoWindow();

  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch({
    location: ourlocation,
    radius: 5000,
    types: ['food']
  }, callbackMap);
}

function callbackMap(results, status) {
  if (status === google.maps.places.PlacesServiceStatus.OK) {
  	successCallBack(results);
    for (var i = 0; i < results.length; i++) {
      //if(results[i].name=== hotelName){
        extractPlaceInfo(results[i]);
      //}
        
        createMarker(results[i]);
    }
    
  }else{
      failureCallBack(results);
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  console.log('place:'+place);
  console.log('placeLoc:'+placeLoc);
  extractPlaceInfo(place);
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

function extractPlaceInfo(place){
  console.log('coming inside extractPlaceInfo');
  var placeID = place.place_id;
  console.log('place.name:'+place.name);
  console.log('placeID'+placeID+'::place.name::'+place.name);
  getPlaceDetails(placeID);  

}

function getPlaceDetails(placeID){
  /*var URL = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=';
  URL = URL + placeID +'&key=AIzaSyCa21Ka_0UGfxSkH7OPhaT8GDA2YMkBWQ0';
  console.log(':: URL ::'+URL+'::');
*/
 /* $.get(URL, function(data) {
  console.log('data:'+data);
  
  });*/

  var request = {
   placeId: placeID
  };

  service = new google.maps.places.PlacesService(map);
  service.getDetails(request, callbackPlaceDetails);

}

function callbackPlaceDetails(place, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
        printPlaceDetails(place);
    }
  }

function printPlaceDetails(data){
  console.log('coming inside printPlaceDetails'+data.place_id);
  var reviews = data.reviews;
  if(reviews!= null && reviews.length>0){
    console.log('num reviews:' + reviews.length);
    for (var i = 0; i < reviews.length; i++) {
      console.log('review'+i+':'+reviews[i]);
      var aspects = reviews[i].aspects;
      console.log('aspects size:'+aspects.length);

      for (var j = 0; j < aspects.length; j++) {

        console.log('aspect:'+aspects[j]);
        console.log(aspects[j].type);
        console.log(aspects[j].rating);

      }
      console.log('author_name'+reviews[i].author_name);
      console.log('text'+reviews[i].text);
      console.log('rating'+reviews[i].rating);

    }
  }

}

module.exports = {

  // get daily note
  searchFoodNearCoords: function(latitude, longitude,success,failure) {
  	  initMap(latitude,longitude,success,failure);
      
  },
  searchAgain: function(){
  	initMap();
  }
  /*,
  
  coolMethod = function(arg0, success, error) {
    exec(success, error, "FoodSearch", "coolMethod", [arg0]);
  }*/
};



});
