$(document).on("pagecreate", "#searchResultsPage", function(){

    //Required for getting popup menu and star-rating
    $('#searchResultsPage').append('<script type="text/javascript" src="assets/scripts/jquery-2.1.4.min.js"></script> <script type="text/javascript" src="assets/scripts/jquery.mobile-1.4.5.min.js"></script> <script type="text/javascript" src="assets/scripts/index.js"></script> ');

    //Request data from online API service
    //                      $.getJSON("URL of online API service", function(data){
    var $jsonOnlineRequest = $.getJSON("http://pastebin.com/raw.php?i=Ks4aXdhY", function(data){

          console.log("Online API called");
          
          $('#searchList').append('<ul data-role="listview" data-inset="true">');
          $('#searchList').append('<li data-role="list-divider" role="heading" data-theme="a">Burgers around Chicago</li>');
          $.each(data, function (index, value) {
              $('#searchList').append('<li id="listItem'+value.foodid+'"> <a href="#reviewPopup'+value.foodid+'" data-rel="popup" class="ui-btn ui-btn-d ui-icon-star ui-btn-icon-right" id="button'+value.foodid+'" data-transition="slide"><h3>' + value.name + '</h3><div data-role="controlgroup" data-type="horizontal">  <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> </div></a></li>');
              $('#searchList').append('<li><a href="'+ value.href_navigation +'" class="ui-btn ui-icon-navigation ui-btn-icon-right" ><p>'+value.price+'</p><p><strong>'+value.restaurantName + '</strong></p><p>'+ value.description+'</p></a></li>');
              $('#searchList').append('<div data-role="popup" id="reviewPopup'+value.foodid+'" data-position-to="button'+value.foodid+'" data-overlay-theme="a" class="ui-content" data-theme="a" ><a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" data-rel="back">Delete Icon</a><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><input name="reviewStar'+value.foodid+'" type="radio" class="star" /><a href="#reviewPage" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-notext" data-transition="slide">Comment Icon</a></div>');
              $("#reviewPopup"+value.foodid).css({"width": "125"});
              $("#button"+value.foodid).click(function () {
                  $("#reviewPopup"+value.foodid).popup("open");
              });
          });

          $('#searchList').append('</ul>');

    });

    //Update indexed db after fetching data from online API service
    $jsonOnlineRequest.then(function(){
          var db;
          //Opening database in indexedDB
          var openDB = indexedDB.open("FoodiesDB",2);
          console.log("indexedDB.open is called ");
          openDB.onsuccess = function(e){
              console.log("openDB.onsuccess is called ");
              db = e.target.result;
              openTransaction();
          }
          openDB.onerror = function(){
              console.log("openDB.onerror is called");
          }
          //Creating dataStore in indexedDB and upgrade of indexedDB
          openDB.onupgradeneeded = function(e){
              console.log("openDB.onupgradeneeded is called");
              var upgradeDB = e.target.result;
              if (!upgradeDB.objectStoreNames.contains("Food")) {
                  var dataStore = upgradeDB.createObjectStore("Food", { keyPath: "foodid" });
                  dataStore.createIndex("foodid","foodid", {unique:true});
                  console.log("Created objectStore and index");
              }
          }

          function openTransaction(){
              $.getJSON("http://pastebin.com/raw.php?i=Ks4aXdhY", function(data){
                    var dataStore = db.transaction(["Food"],"readwrite").objectStore("Food");
                    for(i=0; i < data.length; i++){
                        dataStore.put(data[i]);
                    }
              });
          }
    });

    //Upon failing of online API service, fetch data from local indexed db
    $jsonOnlineRequest.fail(function(){
          console.log("Online JSON request failed");
          

          $('#searchList').append('<ul data-role="listview" data-inset="true">');
          $('#searchList').append('<li data-role="list-divider" role="heading" data-theme="a">Burgers around Chicago</li>');
          var db;
          //Opening database in indexedDB
          var openDB = indexedDB.open("FoodiesDB",2);
          // create transaction
          openDB.onsuccess = function(e){
              db = e.target.result;
              var dataStore2 = db.transaction(["Food"],"readonly").objectStore("Food");
      				// define data object store
      				var cursor = dataStore2.openCursor();
      				// do something with return...
      				cursor.onsuccess = function(e) {
      					var result = e.target.result;
                if(result){
                  $('#searchList').append('<div data-role="popup" id="reviewPopup'+result.value.foodid+'" data-position-to="button'+result.value.foodid+'" data-overlay-theme="a" class="ui-content" data-theme="a" ><a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" data-rel="back">Delete Icon</a><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><input name="reviewStar'+result.value.foodid+'" type="radio" class="star" /><a href="#reviewPage" class="ui-btn ui-corner-all ui-icon-comment ui-btn-icon-notext" data-transition="slide">Comment Icon</a></div>');
                }
      					if (result) {
                  $('#searchList').append('<li id="listItem'+result.value.foodid+'"> <a href="#reviewPopup'+result.value.foodid+'" data-rel="popup" class="ui-btn ui-btn-d ui-icon-star ui-btn-icon-right" id="button'+result.value.foodid+'" data-transition="slide"><h3>' + result.value.name + '</h3><div data-role="controlgroup" data-type="horizontal">  <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> <input name="reviewStarUneditable'+result.value.foodid+'" type="radio" class="star star-rating-on" disabled="disabled" /> </div></a></li>');
                  $('#searchList').append('<li><a href="'+ result.value.href_navigation +'" class="ui-btn ui-icon-navigation ui-btn-icon-right" ><p>'+result.value.price+'</p><p><strong>'+result.value.restaurantName + '</strong></p><p>'+ result.value.description+'</p></a></li>');

                  $("#reviewPopup"+result.value.foodid).css({"width": "125"});
                  $("#button"+result.value.foodid).click(function () {
                      $("#reviewPopup"+result.value.foodid).popup();
                      $("#reviewPopup"+result.value.foodid).popup("open");
                  });
      				    result.continue();
      					}
      				}
          }

          $('#searchList').append('</ul>');

    });
});
